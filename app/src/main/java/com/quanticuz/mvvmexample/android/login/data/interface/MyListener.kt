package com.quanticuz.mvvmexample.android.login.data.`interface`

import android.util.Log
import com.quanticuz.mvvmexample.android.login.data.model.LoginInfo

class MyListener {
    fun onClickButton(loginInfo: LoginInfo){
        Log.d("MyListener","LoginInfo:::\t\t"+loginInfo.userName+":::::"+loginInfo.userPassWord)
    }
}