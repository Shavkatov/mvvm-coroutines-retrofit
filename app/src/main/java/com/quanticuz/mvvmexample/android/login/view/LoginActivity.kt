package com.quanticuz.mvvmexample.android.login.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.quanticuz.mvvmexample.android.R

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportFragmentManager.beginTransaction()
            .replace(R.id.container_basic_login, LoginFragment()).commit()
    }

}