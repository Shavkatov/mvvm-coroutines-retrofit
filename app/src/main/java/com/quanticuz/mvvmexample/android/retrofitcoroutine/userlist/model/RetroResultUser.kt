package com.quanticuz.mvvmexample.android.retrofitcoroutine.userlist.model

data class RetroResultUser(val data : Data , val ad: Ad)