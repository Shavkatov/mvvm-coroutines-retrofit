package com.quanticuz.mvvmexample.android.retrofitcoroutine.login.model

data class LoginModel(val email: String, val password: String)
{
}