package com.quanticuz.mvvmexample.android.retrofitcoroutine.di

import com.quanticuz.mvvmexample.android.retrofitcoroutine.login.viewmodel.LoginViewModelFactory
import com.quanticuz.mvvmexample.android.retrofitcoroutine.userlist.viewmodel.UserListViewModelFactory
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkModule::class])
interface NetworkComponent {
    fun inject(loginViewModelFactory: LoginViewModelFactory)
    fun inject(userListViewModelFactory: UserListViewModelFactory)
}