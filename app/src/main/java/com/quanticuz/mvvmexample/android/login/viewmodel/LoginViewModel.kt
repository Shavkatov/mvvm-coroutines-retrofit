package com.quanticuz.mvvmexample.android.login.viewmodel


import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.quanticuz.mvvmexample.android.login.repository.ValidationRepository

class LoginViewModel: AndroidViewModel {
    private var validationRepository: ValidationRepository

    constructor(application: Application) : super(application){
        validationRepository = ValidationRepository(application)
    }

    fun validateCredentials(email:String,passWord:String): LiveData<String> {
        return validationRepository.validateCredentials(email,passWord)
    }
}
