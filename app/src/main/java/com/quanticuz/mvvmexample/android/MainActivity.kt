package com.quanticuz.mvvmexample.android

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.quanticuz.mvvmexample.android.login.view.LoginActivity

class MainActivity : AppCompatActivity() {

    private lateinit var loginMVVM: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loginMVVM = findViewById(R.id.login_mvvm)
        loginMVVM.setOnClickListener {
            startActivity(Intent(this,LoginActivity::class.java))
        }

    }
}