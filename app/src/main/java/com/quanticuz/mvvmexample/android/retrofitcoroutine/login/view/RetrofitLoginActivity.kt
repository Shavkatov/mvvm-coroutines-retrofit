package com.quanticuz.mvvmexample.android.retrofitcoroutine.login.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.quanticuz.mvvmexample.android.R

class RetrofitLoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_retrofit_login)
    }
}