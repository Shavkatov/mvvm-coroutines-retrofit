package com.quanticuz.mvvmexample.android.retrofitcoroutine.userlist

import com.quanticuz.mvvmexample.android.retrofitcoroutine.userlist.model.Data

interface ItemClickListener {
    fun setClickedInfo(data: Data)
}