package com.quanticuz.mvvmexample.android.retrofitcoroutine.network

import com.quanticuz.mvvmexample.android.retrofitcoroutine.login.model.LoginModel
import com.quanticuz.mvvmexample.android.retrofitcoroutine.login.model.TokenModel
import com.quanticuz.mvvmexample.android.retrofitcoroutine.userlist.model.RetroResult
import com.quanticuz.mvvmexample.android.retrofitcoroutine.userlist.model.RetroResultUser
import retrofit2.Response
import retrofit2.http.*

interface NetworkAPIService {
    @POST("/api/login")
    suspend fun validateLogin(@Body loginModel: LoginModel): Response<TokenModel>

    @GET("/api/users")
    suspend fun fetchUsers(@Query("page") page: Int): Response<RetroResult>

    @GET("/api/users/{id}")
    suspend fun fetchSelectedUsers(@Path("id") id: Int): Response<RetroResultUser>
}