@file:Suppress("DEPRECATION")

package com.quanticuz.mvvmexample.android.login.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.quanticuz.mvvmexample.android.LoginFragmentBinding
import com.quanticuz.mvvmexample.android.R
import com.quanticuz.mvvmexample.android.login.data.`interface`.MyListener
import com.quanticuz.mvvmexample.android.login.viewmodel.LoginViewModel

@Suppress("DEPRECATION")
class LoginFragment : Fragment() {
    private lateinit var loginView: View
    private lateinit var loginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginViewModel = ViewModelProviders.of(requireActivity())
            .get(LoginViewModel::class.java)
    }

    lateinit var myListener: MyListener
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val loginBinder: LoginFragmentBinding =
            DataBindingUtil.inflate(inflater, R.layout.login_data_binding, container, false)
        loginView = loginBinder.root
        myListener = MyListener()
//        loginBinder.han = myListener
        loginBinder.loginbtn.setOnClickListener(View.OnClickListener {
            Log.d("LoginDatabinding", loginBinder.editTextemailID.getText().toString())
            val email: String = loginBinder.editTextemailID.getText().toString()
            val password: String = loginBinder.editTextpassID.text.toString()
            loginViewModel.validateCredentials(email, password)
                .observe(viewLifecycleOwner,
                    { t -> Toast.makeText(activity, t, Toast.LENGTH_LONG).show() })
        })
        // loginView = inflater.inflate(R.layout.login_data_binding,container,false)
        return loginView
    }

}
